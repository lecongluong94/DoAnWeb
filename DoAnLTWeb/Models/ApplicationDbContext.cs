﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DoAnLTWeb.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<AmThuc> AmThuc { get; set; }
        public DbSet<DiaDiem> DiaDiem { get; set; }
        public DbSet<Phuot> Phuot { get; set; }
        public DbSet<DoDay> DoDay { get; set; }
        public ApplicationDbContext()
            : base("DoAnLTWebConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}