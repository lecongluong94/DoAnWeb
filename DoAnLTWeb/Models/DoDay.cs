﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DoAnLTWeb.Models
{
    public class DoDay
    {
        [Key]
        public int Id { get; set; }
        public string TieuDe { get; set; }
        public string HinhAnh { get; set; }
        public DateTime NgayDang { get; set; }

    }
}