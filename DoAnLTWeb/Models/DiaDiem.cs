﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DoAnLTWeb.Models
{
    public class DiaDiem
    {
        [Key]
        public int Id { get; set; }
        public string TenDiaDiem { get; set; }
        public string TinhTP { get; set; }
        public string TieuDe { get; set; }
        public string NoiDung { get; set; }
        public string HinhAnh { get; set; }
        public string GhiChu { get; set; }
        public DateTime NgayDang { get; set; }
    }
}