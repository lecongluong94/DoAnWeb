﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DoAnLTWeb.Models
{
    public class AmThuc
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string TenMon { get; set; }
        public string TinhTP { get; set; }
        [Required]
        public string TieuDe { get; set; }
        [Required]
        public string NoiDung { get; set; }
        public string HinhAnh { get; set; }
        public DateTime NgayDang { get; set; }
        public string GhiChu { get; set; }
    }
}