﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DoAnLTWeb.Models;
using Microsoft.AspNet.Identity;

namespace DoAnLTWeb.Controllers
{
    public class AmThucsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: AmThucs
        public ActionResult Index()
        {
            return View(db.AmThuc.ToList());
        }

        // GET: AmThucs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AmThuc amThuc = db.AmThuc.Find(id);
            if (amThuc == null)
            {
                return HttpNotFound();
            }
            return View(amThuc);
        }

        // GET: AmThucs/Create
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TenMon,TinhTP,TieuDe,NoiDung,NgayDang,GhiChu")] AmThuc amThuc, HttpPostedFileBase UploadImage)
        {
            if (ModelState.IsValid)
            {
                if (UploadImage != null)
                {

                    if (UploadImage.ContentType == "image/jpg" || UploadImage.ContentType == "image/png" ||
                        UploadImage.ContentType == "image/bmp" || UploadImage.ContentType == "image/gif" ||
                        UploadImage.ContentType == "image/jpeg")
                    {
                        UploadImage.SaveAs(Server.MapPath("/") + "/Content/" + UploadImage.FileName);
                        amThuc.HinhAnh = UploadImage.FileName;
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return View();
                }
                db.AmThuc.Add(amThuc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(amThuc);
        }

        // GET: AmThucs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AmThuc amThuc = db.AmThuc.Find(id);
            if (amThuc == null)
            {
                return HttpNotFound();
            }
            return View(amThuc);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TenMon,TinhTP,TieuDe,NoiDung,HinhAnh,NgayDang,GhiChu")] AmThuc amThuc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(amThuc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(amThuc);
        }

        // GET: AmThucs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AmThuc amThuc = db.AmThuc.Find(id);
            if (amThuc == null)
            {
                return HttpNotFound();
            }
            return View(amThuc);
        }

        // POST: AmThucs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AmThuc amThuc = db.AmThuc.Find(id);
            db.AmThuc.Remove(amThuc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
