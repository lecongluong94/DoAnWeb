﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DoAnLTWeb.Models;

namespace DoAnLTWeb.Controllers
{
    public class PhuotsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Phuots
        public ActionResult Index()
        {
            return View(db.Phuot.ToList());
        }

        // GET: Phuots/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Phuot phuot = db.Phuot.Find(id);
            if (phuot == null)
            {
                return HttpNotFound();
            }
            return View(phuot);
        }

        // GET: Phuots/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TieuDe,NoiDung,HinhAnh,NgayDang,GhiChu")] Phuot phuot, HttpPostedFileBase UploadImage)
        {
            if (ModelState.IsValid)
            {
                if (UploadImage != null)
                {

                    if (UploadImage.ContentType == "image/jpg" || UploadImage.ContentType == "image/png" ||
                        UploadImage.ContentType == "image/bmp" || UploadImage.ContentType == "image/gif" ||
                        UploadImage.ContentType == "image/jpeg")
                    {
                        UploadImage.SaveAs(Server.MapPath("/") + "/Content/" + UploadImage.FileName);
                        phuot.HinhAnh = UploadImage.FileName;
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return View();
                }
                db.Phuot.Add(phuot);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(phuot);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Phuot phuot = db.Phuot.Find(id);
            if (phuot == null)
            {
                return HttpNotFound();
            }
            return View(phuot);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TieuDe,NoiDung,HinhAnh,NgayDang,GhiChu")] Phuot phuot)
        {
            if (ModelState.IsValid)
            {
                db.Entry(phuot).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(phuot);
        }

        // GET: Phuots/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Phuot phuot = db.Phuot.Find(id);
            if (phuot == null)
            {
                return HttpNotFound();
            }
            return View(phuot);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Phuot phuot = db.Phuot.Find(id);
            db.Phuot.Remove(phuot);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
