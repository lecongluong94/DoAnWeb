﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DoAnLTWeb.Models;

namespace DoAnLTWeb.Controllers
{
    public class DoDaysController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: DoDays
        public ActionResult Index()
        {
            return View(db.DoDay.ToList());
        }

        // GET: DoDays/Details/5
        public ActionResult ChiTiet(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DoDay doDay = db.DoDay.Find(id);
            if (doDay == null)
            {
                return HttpNotFound();
            }
            return View(doDay);
        }

        // GET: DoDays/Create
        public ActionResult Them()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Them([Bind(Include = "Id,TieuDe,HinhAnh,NgayDang")] DoDay doDay)
        {
            if (ModelState.IsValid)
            {
                db.DoDay.Add(doDay);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(doDay);
        }

        // GET: DoDays/Edit/5
        public ActionResult Sua(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DoDay doDay = db.DoDay.Find(id);
            if (doDay == null)
            {
                return HttpNotFound();
            }
            return View(doDay);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Sua([Bind(Include = "Id,TieuDe,NgayDang")] DoDay doDay)
        {
            if (ModelState.IsValid)
            {
                db.Entry(doDay).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(doDay);
        }

        // GET: DoDays/Delete/5
        public ActionResult Xoa(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DoDay doDay = db.DoDay.Find(id);
            if (doDay == null)
            {
                return HttpNotFound();
            }
            return View(doDay);
        }

        // POST: DoDays/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DoDay doDay = db.DoDay.Find(id);
            db.DoDay.Remove(doDay);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
